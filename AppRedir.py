import socket
import random

def select_random_url(urls):
    return random.choice(urls)

def process_request(clientsocket, addr, urls):
    try:
        url = select_random_url(urls)
        print(f"Conexión establecida con: {addr}")
        request = clientsocket.recv(2048).decode('utf-8')
        print(f"Solicitud recibida:\n{request}")

        response = f"HTTP/1.1 302 Moved Temporarily\r\nLocation: {url}\r\n\r\n"
        print("Respuesta HTTP enviada al cliente:")
        print(response)

        clientsocket.sendall(response.encode('utf-8'))
    except Exception as e:
        print(f"Error al procesar la solicitud: {e}")
    finally:
        clientsocket.close()

def start_server(port, urls):
    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysocket.bind(('localhost', port))
    mysocket.listen(5)
    print(f"Servidor escuchando en el puerto {port}")

    try:
        while True:
            clientsocket, addr = mysocket.accept()
            process_request(clientsocket, addr, urls)
    except KeyboardInterrupt:
        print("Saliendo...")
    finally:
        mysocket.close()
        print("Servidor cerrado.")

if __name__ == '__main__':
    urls = [
        "https://www.aulavirtual.urjc.es/moodle/",
        "http://www.google.com/",
        "https://www.urjc.es/intranet-urjc",
        "http://www.youtube.com/",
        "http://www.twitter.com/",
        "http://www.wikipedia.org/",
    ]
    PORT = 1234
    start_server(PORT, urls)